const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll(){
        const items = FighterRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    
    create(data){
        return FighterRepository.create(data);
    }

    delete(id){
        return FighterRepository.delete(id);
    }

    update(id, data){
        if(this.search({id})){
            return FighterRepository.update(id, data);
        } else {
            return null;
        }
    }
    
}

module.exports = new FighterService();