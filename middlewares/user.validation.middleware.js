const { user } = require('../models/user');
const { checkOddFields, checkIfHasReqFields} = require("./utils/reqValidation");
const UserReqTemplate={
    ...user,
}
delete UserReqTemplate.id;

exports.createUserValid = (req, res, next) => {
    const newUser = req.body;

    try {
        checkOddFields(newUser, UserReqTemplate);
        checkIfHasReqFields(newUser, UserReqTemplate);
        checkFieldsValidation(newUser);
    } catch (err) {
        req.err = {
            code: 400,
            message: err.message,
        }
    } finally {
        next();
    }

}

exports.updateUserValid = (req, res, next) => {
    const newUser = req.body;

    try {
        checkOddFields(newUser, UserReqTemplate);
        checkFieldsValidation(newUser);
    } catch (err) {
        req.err = {
            code: 400,
            message: err.message,
        }
    } finally {
        next();
    }
}


// Utils


function checkFieldsValidation(reqBody) {
    const {
        email,
        phoneNumber,
        password
    } = reqBody;

    if(email) validateEmail(email);
    if(phoneNumber) validatePhone(phoneNumber);
    if(password) validatePassword(password);
}

function validateEmail(email) {
    if (!email.trim().match(/\S+@gmail\.com$/g)) {
        throw new Error("Wrong email format");
    }
}

function validatePhone(phoneNumber) {
    
    if (!phoneNumber.trim().match(/^\+38(0\d{9})$/)) {
        throw new Error("Wrong phone format");
    }
}

function validatePassword(email) {
    if (email.length <= 3) {
        throw new Error("Too short Password, min 3 symbs required");
    }
}