const { fighter } = require('../models/fighter');
const { checkOddFields, checkIfHasReqFields, checkFieldRange} = require("./utils/reqValidation");

const FighterReqTemplate={
    ...fighter,
}
delete FighterReqTemplate.id;

const createFighterValid = (req, res, next) => {
    const newFighter = req.body;

    try {
        checkOddFields(newFighter, FighterReqTemplate);
        checkIfHasReqFields(newFighter, FighterReqTemplate);
        checkFieldsValidation(newFighter);
    } catch (err) {
        req.err = {
            code: 400,
            message: err.message,
        }
    } finally {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    const newFighter = req.body;

    try {
        checkOddFields(newFighter, FighterReqTemplate);
        checkFieldsValidation(newFighter);
    } catch (err) {
        req.err = {
            code: 400,
            message: err.message,
        }
    } finally {
        next();
    }
}

function checkFieldsValidation(reqBody) {
    const {
        // health,
        power,
        defense
    } = reqBody;
    
    if(power) checkFieldRange(power, 0, 99, "power");
    if(defense) checkFieldRange(defense, 0, 9, "defense");
    // if(health) checkFieldRange(health, 0, 1000, "health");
}


exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;