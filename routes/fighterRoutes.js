const {
    Router
} = require('express');
const FighterService = require('../services/fighterService');
const {
    responseMiddleware
} = require('../middlewares/response.middleware');
const {
    createFighterValid,
    updateFighterValid
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get("/", (req, res, next) => {
    const data = FighterService.getAll();

    if (!data || !data.length) {
        req.err = {
            code: 404,
            message: "fighters not found"
        }
    }
    res.data = data;
    next();
}, responseMiddleware);


router.get("/:id", (req, res, next) => {
    const {
        id
    } = req.params;

    const data = FighterService.search({
        id
    });

    if (!data) {
        req.err = {
            code: 404,
            message: "fighter not found"
        }
    }
    res.data = data;

    next();

}, responseMiddleware);

router.delete("/:id", (req, res, next) => {
    const {
        id
    } = req.params;

    const data = FighterService.delete(id);

    if (!data.length) {
        req.err = {
            code: 404,
            message: "fighter is not found"
        }
    }
    res.data = data;

    next();

}, responseMiddleware);

router.post("/", createFighterValid, (req, res, next) => {

    if (req.err) {
        next();
        return;
    }
    const data = FighterService.create(req.body);
    res.data = data;

    next();

}, responseMiddleware);

router.put("/:id", updateFighterValid, (req, res, next) => {
    if (req.err) {
        next();
        return;
    }
    const {
        id
    } = req.params;

    const data = FighterService.update(id, req.body);

    if (!data) {
        req.err = {
            code: 404,
            message: "fighter is not found"
        }
    }
    res.data = data;

    next();


}, responseMiddleware);


module.exports = router;